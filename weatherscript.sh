#!/bin/bash

source weather.config

while true ; do
	wget http://meteo.by/minsk/
	cat index.html | tr -d '\r\n ' > without_spaces.txt
	egrep -o "<pclass=\"t\"><strong>.[0-9]+" without_spaces.txt | egrep -o ".[0-9]+"
	rm index.html without_spaces.txt
	sleep $timeout
done
